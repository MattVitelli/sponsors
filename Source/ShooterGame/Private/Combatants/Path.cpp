// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"

APath::APath(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

TArray<FVector> APath::GetTargetPoints()
{
    TArray<FVector> outPoints;
    for(auto target : PathTargetPoints)
    {
        outPoints.Add(target->GetActorLocation());
    }
    return outPoints;
}