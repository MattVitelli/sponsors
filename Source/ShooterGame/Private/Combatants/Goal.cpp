// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#include "ShooterGame.h"

UGoal::UGoal(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

GoalState UGoal::GetGoalState() const
{
    return GoalState::IN_PROGRESS;
}

void UGoal::OnGoalSuccess()
{
    
}

void UGoal::OnGoalFailure()
{
    
}