// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#include "ShooterGame.h"

UAttackGoal::UAttackGoal(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

GoalState UAttackGoal::GetGoalState() const
{
    return GoalState::IN_PROGRESS;
}

void UAttackGoal::OnGoalSuccess()
{
    
}

void UAttackGoal::OnGoalFailure()
{
    
}