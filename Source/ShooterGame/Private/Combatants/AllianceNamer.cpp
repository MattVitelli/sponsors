#include "ShooterGame.h"
#include "../../Classes/Combatants/AllianceNamer.h"

AllianceNamer::AllianceNamer() {}
AllianceNamer::~AllianceNamer() {}

const int32 NUM_ADJ = 7;
const int32 NUM_NOUN = 6;

const FString BL_ADJ[7] = {
	"Red",
	"Bloody",
	"Deadly",
	"Wrathful",
	"Satanic",
	"Gruesome",
	"Chaotic"
};

const FString JUST_ADJ[7] = {
	"Blue",
	"Ardent",
	"Fervent",
	"Just",
	"Righteous",
	"Godly",
	"Merciful"
};

const FString IND_ADJ[7] = {
	"Black",
	"Lone",
	"Mysterious",
	"Shady",
	"Silent",
	"Distant",
	"Reserved"
};

const FString EGO_ADJ[7] = {
	"Green",
	"Boastful",
	"Loud",
	"Flashy",
	"Famous",
	"Charismatic",
	"Charming"
};

const FString SURV_ADJ[7] = {
	"Yellow",
	"Skittish",
	"Lasting",
	"Prudent",
	"Undying",
	"Elusive",
	"Slippery"
};

const FString BL_NOUN[6] = {
	"Demons",
	"Hellions",
	"Berserkers",
	"Destroyers",
	"Ghouls",
	"Psychopaths"
};

const FString JUST_NOUN[6] = {
	"Shields",
	"Knights",
	"Protectors",
	"Guardians",
	"Heroes",
	"Defenders"
};

const FString IND_NOUN[6] = {
	"Rovers",
	"Ronin",
	"Rangers",
	"Hermits",
	"Outcasts",
	"Riders"
};

const FString EGO_NOUN[6] = {
	"Stars",
	"Braggarts",
	"Champions",
	"Idols",
	"Celebrities",
	"Leaders"
};

const FString SURV_NOUN[6] = {
	"Survivalists",
	"Cockroaches",
	"Abiders",
	"Adaptors",
	"Waters",
	"Cowards"
};

FString AllianceNamer::getAdj(AllianceAttributeType type) {
	int32 index = FMath::RandHelper(NUM_ADJ);
	if (type == ALLIANCE_BLOODLUST) return BL_ADJ[index];
	if (type == ALLIANCE_JUSTICE) return JUST_ADJ[index];
	if (type == ALLIANCE_INDEPENDENCE) return IND_ADJ[index];
	if (type == ALLIANCE_LOYALTY) return "Loyal";
	if (type == ALLIANCE_EGO) return EGO_ADJ[index];
	if (type == ALLIANCE_SURVIVAL) return SURV_ADJ[index];
	return "Unknown";
}

FString AllianceNamer::getNoun(AllianceAttributeType type) {
	int32 index = FMath::RandHelper(NUM_NOUN);
	if (type == ALLIANCE_BLOODLUST) return BL_NOUN[index];
	if (type == ALLIANCE_JUSTICE) return JUST_NOUN[index];
	if (type == ALLIANCE_INDEPENDENCE) return IND_NOUN[index];
	if (type == ALLIANCE_LOYALTY) return "Friends";
	if (type == ALLIANCE_EGO) return EGO_NOUN[index];
	if (type == ALLIANCE_SURVIVAL) return SURV_NOUN[index];
	return "Errors";
}