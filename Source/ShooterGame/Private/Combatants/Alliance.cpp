// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Engine.h"
#include "../../Classes/Missions/AllianceWarRoom.h"
#include "../../Classes/Combatants/Combatant.h"
#include "../../Classes/Combatants/AllianceNamer.h"

// TESTING...
#include "../../Classes/Missions/KillGoal.h"

TArray<AAlliance*> AAlliance::allAlliances = TArray<AAlliance*>();

void AAlliance::BeginPlay()
{
	Super::BeginPlay();

	GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("" + this->name() + " has " + FString::SanitizeFloat(this->allianceMembers.Num()) + " members."));
	AAlliance::allAlliances.Add(this);
	GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Green, TEXT("There are " + FString::SanitizeFloat(AAlliance::allAlliances.Num()) + " alliances."));

	UKillGoal* testGoal = NewObject<UKillGoal>();
	testGoal->myAlliance = this;
	testGoal->optimize();
	if (testGoal->target()) GEngine->AddOnScreenDebugMessage(1, 5.0, FColor::Red, TEXT("" + this->name() + " wants to attack " + testGoal->target()->name() + "."));
	GEngine->AddOnScreenDebugMessage(2, 5.0, FColor::Red, TEXT("Bloodlust: " + FString::SanitizeFloat(testGoal->rawUtilPoints(ALLIANCE_BLOODLUST))));
	GEngine->AddOnScreenDebugMessage(3, 5.0, FColor::Red, TEXT("Justice: " + FString::SanitizeFloat(testGoal->rawUtilPoints(ALLIANCE_JUSTICE))));
	GEngine->AddOnScreenDebugMessage(4, 5.0, FColor::Red, TEXT("Ego: " + FString::SanitizeFloat(testGoal->rawUtilPoints(ALLIANCE_EGO))));
	GEngine->AddOnScreenDebugMessage(5, 5.0, FColor::Red, TEXT("Independence: " + FString::SanitizeFloat(testGoal->rawUtilPoints(ALLIANCE_INDEPENDENCE))));
	GEngine->AddOnScreenDebugMessage(6, 5.0, FColor::Red, TEXT("Survival: " + FString::SanitizeFloat(testGoal->rawUtilPoints(ALLIANCE_SURVIVAL))));
}

void AAlliance::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	Super::EndPlay(EndPlayReason);

	prevPrimary = ALLIANCE_NONE;
	prevSecondary = ALLIANCE_NONE;
	adj = "Uninitialized";
	noun = "Defaults";

	for (auto member : allianceMembers) { member->OnDeathEvent.RemoveUObject(this, &AAlliance::OnMemberDeath); }
	allianceMembers = TArray<AShooterCharacter*>();


	AAlliance::allAlliances.Remove(this);
}

void AAlliance::Destroyed() {
	AAlliance::allAlliances.Remove(this);
	warRoom = NULL;

	AActor::Destroyed();
}

FString AAlliance::name()
{
    return adj + " " + noun;
}

void AAlliance::OnJoinedAlliance_Implementation(AShooterCharacter *newMember)
{
    //Can only join alliances once
    if(allianceMembers.Contains(newMember))
        return;
    
    allianceMembers.Add(newMember);
	newMember->OnDeathEvent.AddUObject(this, &AAlliance::OnMemberDeath);
    checkForNewName();

	GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Blue, TEXT("" + this->name() + " gains a member!"));
    
    //TODO - Trigger audio/visual events
}

void AAlliance::OnLeftAlliance_Implementation(AShooterCharacter *oldMember)
{
    //Ensure oldMember is really part of the alliance
    if(!allianceMembers.Contains(oldMember))
        return;
    allianceMembers.Remove(oldMember);
	oldMember->OnDeathEvent.RemoveUObject(this, &AAlliance::OnMemberDeath);
	checkForNewName();

	if (allianceMembers.Num() == 0) OnAllianceDisbandEvent.Broadcast(this);
    //TODO - Trigger audio/visual events
}

void AAlliance::checkForNewName() {
	if (allianceMembers.Num() <= 0) return;
	AllianceAttributeType largestAttribute = ALLIANCE_NONE;
	float largestMagnitude = 0.0;
	AllianceAttributeType secondLargestAttribute = ALLIANCE_NONE;
	float secondLargestMagnitude = 0.0;

	float val = wBloodlust();
	if (val >= largestMagnitude) {
		secondLargestAttribute = largestAttribute;
		secondLargestMagnitude = largestMagnitude;
		largestAttribute = ALLIANCE_BLOODLUST;
		largestMagnitude = val;
	}
	else if (val > secondLargestMagnitude) {
		secondLargestAttribute = ALLIANCE_BLOODLUST;
		secondLargestMagnitude = val;
	}
	val = wJustice();
	if (val >= largestMagnitude) {
		secondLargestAttribute = largestAttribute;
		secondLargestMagnitude = largestMagnitude;
		largestAttribute = ALLIANCE_JUSTICE;
		largestMagnitude = val;
	}
	else if (val > secondLargestMagnitude) {
		secondLargestAttribute = ALLIANCE_JUSTICE;
		secondLargestMagnitude = val;
	}
	val = wIndependence();
	if (val >= largestMagnitude) {
		secondLargestAttribute = largestAttribute;
		secondLargestMagnitude = largestMagnitude;
		largestAttribute = ALLIANCE_INDEPENDENCE;
		largestMagnitude = val;
	}
	else if (val > secondLargestMagnitude) {
		secondLargestAttribute = ALLIANCE_INDEPENDENCE;
		secondLargestMagnitude = val;
	}
	val = wEgo();
	if (val >= largestMagnitude) {
		secondLargestAttribute = largestAttribute;
		secondLargestMagnitude = largestMagnitude;
		largestAttribute = ALLIANCE_EGO;
		largestMagnitude = val;
	}
	else if (val > secondLargestMagnitude) {
		secondLargestAttribute = ALLIANCE_EGO;
		secondLargestMagnitude = val;
	}
	val = wSurvival();
	if (val >= largestMagnitude) {
		secondLargestAttribute = largestAttribute;
		secondLargestMagnitude = largestMagnitude;
		largestAttribute = ALLIANCE_SURVIVAL;
		largestMagnitude = val;
	}
	else if (val > secondLargestMagnitude) {
		secondLargestAttribute = ALLIANCE_SURVIVAL;
		secondLargestMagnitude = val;
	}

	if (largestAttribute != prevPrimary) {
		prevPrimary = largestAttribute;
		noun = AllianceNamer::getNoun(largestAttribute);
	}
	if (secondLargestAttribute != prevSecondary) {
		prevSecondary = secondLargestAttribute;
		adj = AllianceNamer::getAdj(secondLargestAttribute);
	}
}

float wbl(ACombatant* c) { return c->W_BLOODLUST; };
float wjust(ACombatant* c) { return c->W_JUSTICE; };
float wind(ACombatant* c) { return c->W_INDEPENDENCE; };
float wego(ACombatant* c) { return c->W_EGO; };
float wsurv(ACombatant* c) { return c->W_SURVIVAL; };
float wloy(ACombatant* c) { return c->W_LOYALTY; };

float getjustice(ACombatant* c) { return c->GetJusticeScore(); }
float getbloodlust(ACombatant* c) { return c->GetBloodlustScore(); }
float getindependence(ACombatant* c) { return c->GetIndependenceScore(); }

float AAlliance::wBloodlust() { return calcWStat(&wbl); }
float AAlliance::wJustice() { return calcWStat(&wjust); }
float AAlliance::wIndependence() { return calcWStat(&wind); }
float AAlliance::wEgo() { return calcWStat(&wego); }
float AAlliance::wSurvival() { return calcWStat(&wsurv); }

float AAlliance::wLoyalty() { return calcWStat(&wloy); }

float AAlliance::GetJusticeScore() { return calcWStat(&getjustice); }
float AAlliance::GetBloodlustScore() { return calcWStat(&getbloodlust); }
float AAlliance::GetIndependenceScore() { return calcWStat(&getindependence); }

float AAlliance::calcWStat(WFunction wFn) {
	if (allianceMembers.Num() <= 0) return 0.0;
	float toReturn = 0.0;
	for (auto member : allianceMembers) {
		ACombatant* com = Cast<ACombatant>(member);
		if (com) toReturn += (*wFn)(com);
	}
	toReturn /= allianceMembers.Num();
	return toReturn;
}

void AAlliance::OnMemberDeath(AShooterCharacter* deadMember, AShooterCharacter* killer) {
	deadMember->OnDeathEvent.RemoveUObject(this, &AAlliance::OnMemberDeath);
	ACombatant* com = Cast<ACombatant>(deadMember);

	GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("A member of the " + this->name() + " was killed."));

	if (com) com->LeaveAlliance();

	this->OnMemberDeathEvent.Broadcast(this, killer);
}

FVector AAlliance::getCentroid() {
	FVector centroid = FVector(0, 0, 0);
	if (allianceMembers.Num() == 0) return centroid;
	for (auto member : allianceMembers) {
		centroid += member->GetActorLocation();
	}

	centroid /= allianceMembers.Num();
	return centroid;
}

FVector AAlliance::baseLocation() {
	// TODO: Hook up to actual alliance bases.
	return locationOfBase;
	//return getCentroid();
}

void AAlliance::SetBaseLocation(FVector location){
	locationOfBase = location;
}

float AAlliance::utilsForRawPoints(UtilPointVector upv, FVector missionLocation) { return warRoom->utilsForRawPoints(upv, missionLocation); }

bool AAlliance::FightingIsOver(TArray<ACombatant*> combatants){
	if (combatants.Num() == 0) return true;
	AAlliance *alliance = combatants[0]->GetAlliance();
	for (ACombatant* combatant : combatants){
		if (combatant->GetAlliance() != alliance) return false;
	}
	return true;
}

void AAlliance::regroupCombatants(TArray<ACombatant*> combatants, TArray<FVector> locations, float combatantCentroidRefusalDistance) {
    if (combatants.Num() == 0) return;
    
    // dissolve exisiting alliances
    TArray<AAlliance *> existingAlliances;
    for (ACombatant *combatant: combatants) {
        AAlliance *alliance = combatant->GetAlliance();
        if ((alliance != nullptr) && (!existingAlliances.Contains(alliance))) {
            existingAlliances.Add(alliance);
        }
        combatant->LeaveAlliance();
    }
    for (AAlliance *alliance: existingAlliances) {
        alliance->Destroy();
    }
        
    // run k-means
    int32 k = 1;
	int32 kMax = combatants.Num();
    TArray<TArray<ACombatant *> > clusters;

    
    // start with k = 1 and at each iteration try k-means k times,
    // creating k clusters from randomized starting centroids
    
    // if any cluster is approved any of these k times by all combatants,
    // use that cluster. if we fail to approve clustering
    while (k < kMax) {
        for (int i = 0; i < k; i++) {
			clusters = clusterShootersByPersonalityKMeans(combatants, k, combatantCentroidRefusalDistance);
            // clusterShootersByPersonalityKMeans returns an empty list if the combatants refused to cluster
            if (clusters.Num() != 0) break;
        }
        // it's a nested loop with an inner break. the other option is a goto, and fuck that
        if (clusters.Num() != 0) break;
        k += 1;
    }
	if (clusters.Num() == 0){
		for (ACombatant * combatant : combatants){
			TArray<ACombatant *> singleton;
			singleton.Add(combatant);
			clusters.Add(singleton);
		}
	}
	
    // form new alliances according to clusters
    // this part is bullshit
    ACombatant *firstCombatantForWorldGetting = combatants[0];
    UWorld *world = firstCombatantForWorldGetting->GetWorld();
	int32 NumLocations = locations.Num();
    for (int i = 0; i < clusters.Num(); i++) {
        if (world) {
            TArray<ACombatant *> newAllianceCombatants = clusters[i];
            // do we need to care about Owner, Instigator, or bNoCollisionFail?
            FActorSpawnParameters SpawnInfo;
            SpawnInfo.bNoCollisionFail = true;
            AAlliance *alliance = world->SpawnActor<AAlliance>();
            for (ACombatant *combatant : newAllianceCombatants) {
                combatant->JoinAlliance(alliance);
				GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("I'm in an alliance of this many: " + FString::FromInt(newAllianceCombatants.Num())));
            }
			if (NumLocations > 0){
				alliance->SetBaseLocation(locations[i%NumLocations]);
			}
        }
    }
}

TArray<TArray<ACombatant *> > AAlliance::clusterShootersByPersonalityKMeans(TArray<ACombatant*> combatants, int32 k, float combatantCentroidRefusalDistance) {
    int32 kMaxIterations = 32;
    //float combatantCentroidRefusalDistance = 250.0;
    
    // data structure for combatants to keep track of the centroid they're assigned to
    // makes checking new centroid vs. old centroid easy
    TMap<ACombatant *, int32> clusterMap;
    
    // data structure for centroids to keep track of their combatants
    // redundant data structure makes recomputing centroids cleaner
    TArray<TArray<ACombatant *> > combatantsByCentroid;
    for (int32 i = 0; i < k; i++) {
        TArray<ACombatant*> emptyCombatantList;
        combatantsByCentroid.Add(emptyCombatantList);
    }

    
    // local/temp centroid struct
    struct CombatantKMeansCentroid {
        float bloodlust, justice, ego;
        
        float distance(ACombatant *combatant) {
            float bloodlustDiff = combatant->W_BLOODLUST - bloodlust;
            float justiceDiff = combatant->W_JUSTICE - justice;
            float egoDiff = combatant->W_EGO - ego;
            
            float dist_sq = bloodlustDiff * bloodlustDiff + justiceDiff * justiceDiff + egoDiff * egoDiff;
            return FMath::Sqrt(dist_sq);
        }
    };
    
    // get random shooters to use as initial centroids
    TArray<int32> combatantsAsCentroidsIndexList;
    for (int32 i = 0; i < k; i++) {
        int32 randomCombatantAsCentroidIndex;
        // get a random combatant not already in our list of random combatants
        do {
            randomCombatantAsCentroidIndex = FMath::RandRange(0, combatants.Num() - 1);
        } while (combatantsAsCentroidsIndexList.Contains(randomCombatantAsCentroidIndex));
        combatantsAsCentroidsIndexList.Add(randomCombatantAsCentroidIndex);
    }
    // create centroids out of the random shooters
    TArray<CombatantKMeansCentroid> centroids;
    for (int32 i = 0; i < k; i++) {
        CombatantKMeansCentroid nextCentroid;
        ACombatant *nextCombatant = combatants[combatantsAsCentroidsIndexList[i]];
        nextCentroid.bloodlust = nextCombatant->W_BLOODLUST;
        nextCentroid.justice = nextCombatant->W_JUSTICE;
        nextCentroid.ego = nextCombatant->W_EGO;
        centroids.Add(nextCentroid);
    }
    
    // k-means while loop
    for (int32 curIteration = 0; curIteration < kMaxIterations; curIteration++) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("New iteration: k = " + FString::FromInt(k) + ", iter = " + FString::FromInt(curIteration)));
        bool combatantChangedCluster = false;
        
        // clear all clusters' combatants lists
        for (int32 i = 0; i < k; i++) {
            combatantsByCentroid[i].Empty();
        }
        
        // iterate over all combatants and find the best cluster
        for (ACombatant *combatant : combatants) {
            int bestCentroidIndex = -1;
            float bestCentroidDistance = 10000.0;
            
            for (int32 j = 0; j < k; j++) {
                CombatantKMeansCentroid centroid = centroids[j];
                float centroidDistance = centroid.distance(combatant);
                if (bestCentroidIndex == -1 || centroidDistance < bestCentroidDistance) {
                    bestCentroidDistance = centroidDistance;
                    bestCentroidIndex = j;
                }
            }

            // update centroid that combatant is assigned to
            if ((!clusterMap.Contains(combatant)) || bestCentroidIndex != clusterMap[combatant]) {
                //clusterMap[combatant] = bestCentroidIndex;
				clusterMap.Add(combatant, bestCentroidIndex);
                combatantChangedCluster = true;
            }
            combatantsByCentroid[bestCentroidIndex].Add(combatant);
        }
        
        // recompute centroids
        for (int32 i = 0; i < k; i++) {
            float bloodlustSum = 0.0;
            float justiceSum = 0.0;
            float egoSum = 0.0; // cogito ergo sum
            
            for (ACombatant *combatant : combatantsByCentroid[i]) {
                bloodlustSum += combatant->W_BLOODLUST;
                justiceSum += combatant->W_JUSTICE;
                egoSum += combatant->W_EGO;
            }
            
            int32 numCombatantsInCentroid = combatantsByCentroid[i].Num();
            centroids[i].bloodlust = bloodlustSum / float(numCombatantsInCentroid);
            centroids[i].justice = justiceSum / float(numCombatantsInCentroid);
            centroids[i].ego = egoSum / float(numCombatantsInCentroid);
        }
        
        // k-means converged. Note: must come AFTER recomputing centroids for
        // combatant refusal to cluster calculation
        if (!combatantChangedCluster) break;

    }
    
    // check to see if combatants refused to cluster
    for (ACombatant *combatant : combatants) {
        CombatantKMeansCentroid centroid = centroids[clusterMap[combatant]];
        float centroidDistance = centroid.distance(combatant);
        int32 numCombatantsInCentroid = combatantsByCentroid[clusterMap[combatant]].Num();
		GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("" + FString::SanitizeFloat(centroidDistance)));

        // assuming W_INDEPDENDENCE and W_SURVIVAL in range of 0 to 100
        // where ind is greater than 50, distance increases linearly with num people in cluster greater than 1
        
        // high independence => smaller num combatants
        //  low independence => greater num combatants
        // dist offset on a scale of -1 to 1
        float independenceOffset = 100.0 * (combatant->W_INDEPENDENCE - 50) / 50 * FMath::Sqrt(numCombatantsInCentroid);
        float survivalOffset = -100.0 * (combatant->W_SURVIVAL - 50) / 50 * FMath::Sqrt(numCombatantsInCentroid);
        centroidDistance += independenceOffset + survivalOffset;
        
        
        if (centroidDistance > combatantCentroidRefusalDistance) {
            TArray<TArray<ACombatant*> > empty;
            return empty;
        }
    }
	GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("Non-zero clustring"));
    return combatantsByCentroid;
}