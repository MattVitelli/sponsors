// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "FanAIController.h"
#include "Fan.h"

//class AFanAIController; //This was not being recognized when not included

AFan::AFan(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	AIControllerClass = AFanAIController::StaticClass();

	UpdatePawnMeshes();

	bUseControllerRotationYaw = true;

	FAN_TYPE = EFAN_TYPE::EF_Aggressive;
	W_DESIRED_FEATURE = 0;
	W_FAN_BALANCE = 0;
}

bool AFan::IsFirstPerson() const
{
	return false;
}

void AFan::FaceRotation(FRotator NewRotation, float DeltaTime)
{
	FRotator CurrentRotation = FMath::RInterpTo(GetActorRotation(), NewRotation, DeltaTime, 8.0f);

	Super::FaceRotation(CurrentRotation, DeltaTime);
}

float AFan::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	/*const FVector MyLoc = this->GetActorLocation();
	const float HearingDist = 12000;
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		ACombatant* TestPawn = Cast<ACombatant>(*It);
		if (TestPawn && TestPawn->IsAlive())
		{
			const float DistSq = (TestPawn->GetActorLocation() - MyLoc).Size();
			if (DistSq < HearingDist)
			{
				AShooterAIController *aiCon = Cast<AShooterAIController>(TestPawn->GetController());
				if (aiCon)
				{
					aiCon->SetLastHeardSoundLocation(MyLoc);
				}
			}
		}
	}

	this->PlaySoundFX(AUDIO_TYPES::DAMAGE_CRY);
	*/
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "You shot me!");
	AFanAIController *aiCon = Cast<AFanAIController>(this->GetController());
	if (aiCon){
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "I have a soul.");
	}
	ACombatant *combatant = Cast<ACombatant>(EventInstigator->GetPawn());
	if (combatant){
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "I know who you are.");
	}
	if (aiCon && combatant){
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "And I will remember.");
		aiCon->ShotByCombatant(combatant);
	}
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}