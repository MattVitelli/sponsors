// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "../../Classes/Missions/CombatantGoal.h"

UCombatantGoal::UCombatantGoal(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	for (int i = ALLIANCE_NONE; i < NUM_ALLIANCE_ATTRIBUTE_TYPES; i++) { computedUtilPoints.vec[i] = 0.0; }
}