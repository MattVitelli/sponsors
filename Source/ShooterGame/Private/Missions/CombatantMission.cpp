// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "../../Classes/Missions/CombatantMission.h"

UCombatantMission::UCombatantMission(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	goals = TArray<UCombatantGoal*>();

	teamSize = 1;
	minTeamSize = 1;
	minimumHealth = 50;

	for (int i = ALLIANCE_NONE; i < NUM_ALLIANCE_ATTRIBUTE_TYPES; i++) { computedUtilPoints.vec[i] = 0.0; }
}

void UCombatantMission::recomputeUtilPoints() {
	// TODO: Implement.
}