// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTask_DefendPath::UBTTask_DefendPath(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

EBTNodeResult::Type UBTTask_DefendPath::ExecuteTask(UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory)
{
	UBehaviorTreeComponent* MyComp = OwnerComp;
	AShooterAIController* MyController = MyComp ? Cast<AShooterAIController>(MyComp->GetOwner()) : NULL;
	if (MyController == NULL)
	{
		return EBTNodeResult::Failed;
	}
	
	APawn* MyPawn = MyController->GetPawn();
    ACombatant *MyBot = Cast<ACombatant>(MyPawn);
    
	if (MyBot)
	{
        APath *path = MyBot->GetPath();
        if(path)
        {
            const auto & targets = path->PathTargetPoints;
            int32 randIdx = FMath::FRandRange(0, targets.Num());
            FVector origin = targets[randIdx]->GetActorLocation();
            const float radius = 1000.0f;
            const FVector loc = UNavigationSystem::GetRandomPointInRadius(MyController, origin, radius);
            MyComp->GetBlackboardComponent()->SetValueAsVector(BlackboardKey.GetSelectedKeyID(), loc);
            return EBTNodeResult::Succeeded;
        }
	}

	return EBTNodeResult::Failed;
}
