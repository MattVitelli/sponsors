// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTask_TBagTask::UBTTask_TBagTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

EBTNodeResult::Type UBTTask_TBagTask::ExecuteTask(UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory)
{
	UBehaviorTreeComponent* MyComp = OwnerComp;
	AShooterAIController* MyController = MyComp ? Cast<AShooterAIController>(MyComp->GetOwner()) : NULL;
	if (MyController == NULL)
	{
		return EBTNodeResult::Failed;
	}
	
	APawn* MyPawn = MyController->GetPawn();
    ACombatant *MyBot = Cast<ACombatant>(MyPawn);
    AShooterCharacter* Enemy = MyController->GetEnemy();
    
	if (MyBot && Enemy)
	{
        if(!Enemy->IsAlive())
        {
            FVector loc = Enemy->GetActorLocation();
            const float ClosenessThreshold = 300.0;
            MyComp->GetBlackboardComponent()->SetValueAsVector(BlackboardKey.GetSelectedKeyID(), loc);
            if(FVector::Dist(loc, MyBot->GetActorLocation()) < ClosenessThreshold)
            {
                MyBot->Crouch();
                return EBTNodeResult::Succeeded;
            }
            else
            {
                MyBot->UnCrouch();
            }
        }
        else
        {
            return EBTNodeResult::Failed;
        }

	}
    if(MyBot)
    {
        MyBot->UnCrouch();
    }

	return EBTNodeResult::Failed;
}
