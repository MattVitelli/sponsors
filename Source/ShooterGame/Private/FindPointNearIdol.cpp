// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
//#include "FanAIController.h"
//#include "RandomStream.h"
#include "FindPointNearIdol.h"

UFindPointNearIdol::UFindPointNearIdol(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

EBTNodeResult::Type UFindPointNearIdol::ExecuteTask(UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory)
{
	UBehaviorTreeComponent* MyComp = OwnerComp;
	AFanAIController* MyController = MyComp ? Cast<AFanAIController>(MyComp->GetOwner()) : NULL;
	if (MyController == NULL)
	{
		return EBTNodeResult::Failed;
	}

	APawn* MyBot = MyController->GetPawn();
	AShooterCharacter* Idol = MyController->GetIdol();
	float Favor = MyController->GetFavor();
	float RelativeFavor = MyController->GetRelativeFavor();
	if (Idol && MyBot)
	{
		float HoverRadius = (RelativeFavor > 5.0f) ? 400.f - 100.0f*(RelativeFavor / 5.0f) : 400.f;
        const float MIN_HOVER_DISTANCE = 200;
		if (HoverRadius < MIN_HOVER_DISTANCE) HoverRadius = MIN_HOVER_DISTANCE;
		const FVector ForwardVector = Idol->GetActorForwardVector();
		float Angle = FMath::FRandRange(150.0, 210.0);
		const FVector RotatedVector = ForwardVector.RotateAngleAxis(Angle, FVector(0, 0, 1));
		const FVector Loc = Idol->GetActorLocation() + HoverRadius * RotatedVector;
		if (Loc != FVector::ZeroVector)
		{
			MyComp->GetBlackboardComponent()->SetValueAsVector(BlackboardKey.GetSelectedKeyID(), Loc);
			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
}



