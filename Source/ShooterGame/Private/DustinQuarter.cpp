// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "DustinQuarter.h"

ADustinQuarter::ADustinQuarter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Fans = TArray<AFan *>();
	Combatants = TArray<ACombatant *>();
	Alliances = TArray<AAlliance *>();
}

void ADustinQuarter::PopulateQuarter(TArray<AActor *> InActors){
	Fans.Empty();
	Combatants.Empty();
	Alliances.Empty();
	for (auto member : InActors){
		AFan* Fan = Cast<AFan>(member);
		if (Fan){
			Fans.Add(Fan);
		}
		ACombatant* Combatant = Cast<ACombatant>(member);
		if (Combatant){
			Combatants.Add(Combatant);
		}
		AAlliance* Alliance = Cast<AAlliance>(member);
		if (Alliance){
			Alliances.Add(Alliance);
		}
	}
}

TArray<ACombatant *> ADustinQuarter::GetCombatants(){
	return Combatants;
}