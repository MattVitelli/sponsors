// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Fan.h"
#include "FanAIController.h"
#include "DustinQuarter.h"

AFanAIController::AFanAIController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	BlackboardComp = ObjectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackBoardComp"));

	BrainComponent = BehaviorComp = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));

	bWantsPlayerState = true;

	TimeWithIdol = 0;

	ShotByCombatants = TArray<ACombatant *>();
}

void AFanAIController::ShotByCombatant(ACombatant *combatant){
	if (combatant){
		ShotByCombatants.AddUnique(combatant);
		AAlliance *alliance = combatant->GetAlliance();
		if (alliance){
			for (AShooterCharacter* shooterAlly : alliance->allianceMembers){
				ACombatant* combatantAlly = Cast<ACombatant>(shooterAlly);
				if (combatantAlly){
					ShotByCombatants.AddUnique(combatantAlly);
				}
			}
		}
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "I'll remember alright.");
	}
}


void AFanAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	AFan* Fan = Cast<AFan>(InPawn);

	// start behavior
	if (Fan && Fan->FanBehavior)
	{
		BlackboardComp->InitializeBlackboard(Fan->FanBehavior->BlackboardAsset);

		IdolID = BlackboardComp->GetKeyID("Idol");
		//NeedAmmoKeyID = BlackboardComp->GetKeyID("NeedAmmo");
		FavorID = BlackboardComp->GetKeyID("Favor");
		RelativeFavorID = BlackboardComp->GetKeyID("RelativeFavor");
		IdolIsDyingID = BlackboardComp->GetKeyID("IdolIsDying");
		CombatQuarterID = BlackboardComp->GetKeyID("CombatQuarter");

		BehaviorComp->StartTree(*(Fan->FanBehavior));
	}
}

void AFanAIController::BeginInactiveState()
{
	Super::BeginInactiveState();

	AGameState* GameState = GetWorld()->GameState;

	const float MinRespawnDelay = (GameState && GameState->GameModeClass) ? GetDefault<AGameMode>(GameState->GameModeClass)->MinRespawnDelay : 1.0f;

	GetWorldTimerManager().SetTimer(this, &AFanAIController::RespawnFan, MinRespawnDelay);
}

void AFanAIController::RespawnFan()
{
	//GetWorld()->GetAuthGameMode()->RestartPlayer(this);
}

void AFanAIController::FindFavoriteIdol(){
	APawn* MyBot = GetPawn();
	if (MyBot == NULL)
	{
		return;
	}
	const FVector MyLoc = MyBot->GetActorLocation();
	float BestUtility = -10000.0;
	float SecondBestUtility = -10000.0;
	ACombatant* BestPawn = NULL;
	UObject* currentIdol = BlackboardComp->GetValueAsObject(IdolID);
	ACombatant* currentIdolCombatant = Cast<ACombatant>(currentIdol);
	if (currentIdolCombatant && !currentIdolCombatant->IsAlive())
	{
		if (BlackboardComp)
		{
			BlackboardComp->SetValueAsBool(IdolIsDyingID, true);
		}
		ShotByCombatant(currentIdolCombatant->killedMe);
		//ShotByCombatants.AddUnique(currentIdolCombatant->killedMe);
		/*if (currentIdolCombatant->GetAlliance()){

		}*/
		return;
	}
	else {
		if (BlackboardComp)
		{
			BlackboardComp->SetValueAsBool(IdolIsDyingID, false);
		}
	}
	AFan* MyFan = Cast<AFan>(MyBot);
	UObject* currentQuarter = BlackboardComp->GetValueAsObject(CombatQuarterID);
	ADustinQuarter* currentDustinQuarter = Cast<ADustinQuarter>(currentQuarter);
	if (currentDustinQuarter == NULL){
		return;
	}
	for (auto member : currentDustinQuarter->Combatants)
	{
		ACombatant* TestPawn = Cast<ACombatant>(member);
	/*for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		ACombatant* TestPawn = Cast<ACombatant>(*It);
	*/	
		if (ShotByCombatants.Contains(TestPawn)){
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "Asshole.");
		}
		if (TestPawn && TestPawn->IsAlive() && !ShotByCombatants.Contains(TestPawn))
		{
			float utility = 1.0;
			switch (MyFan->FAN_TYPE){
				case EFAN_TYPE::EF_Aggressive:
					utility += MyFan->W_DESIRED_FEATURE * TestPawn->GetBloodlustScore();
					utility -= MyFan->W_FAN_BALANCE * TestPawn->numBloodlustFans;
					break;
				case EFAN_TYPE::EF_Just:
					utility += MyFan->W_DESIRED_FEATURE * TestPawn->GetJusticeScore();
					utility -= MyFan->W_FAN_BALANCE * TestPawn->numJusticeFans;
					break;
				case EFAN_TYPE::EF_Lone:
					utility += MyFan->W_DESIRED_FEATURE * TestPawn->GetIndependenceScore();
					utility -= MyFan->W_FAN_BALANCE * TestPawn->numLoneRangerFans;
					break;
				default:
					break;
			}
			/*const float DistSq = ((TestPawn->GetActorLocation() - MyLoc).SizeSquared());
			if (utility <= 0.1) utility = 0.1;
			utility /= (DistSq + 1);*/
			if (currentIdolCombatant == TestPawn){
				utility += sqrt(TimeWithIdol/2.0);
			}
			if (utility >= BestUtility){
				SecondBestUtility = BestUtility;
				BestUtility = utility;
				BestPawn = TestPawn;
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "debug msg");
			} else if (utility >= SecondBestUtility){
				SecondBestUtility = utility;
			}
			
		}
	}
	if (currentIdolCombatant != BestPawn){
		TimeWithIdol = 0;
		switch (MyFan->FAN_TYPE){
			case EFAN_TYPE::EF_Aggressive:
				if(BestPawn) BestPawn->AddBloodlustFan();
				if(currentIdolCombatant) currentIdolCombatant->RemoveBloodlustFan();
				break;
			case EFAN_TYPE::EF_Just:
				if(BestPawn) BestPawn->AddJusticeFan();
				if(currentIdolCombatant) currentIdolCombatant->RemoveJusticeFan();
				break;
			case EFAN_TYPE::EF_Lone:
				if (BestPawn) BestPawn->AddLoneRangerFan();
				if (currentIdolCombatant) currentIdolCombatant->RemoveLoneRangerFan();
				break;
			default:
				break;
		}
	} else {
		TimeWithIdol += .5; //Should be the tick time or actually based off a clock
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "debug msg2");
	}
		
	if (BestPawn)
	{
		AShooterCharacter *BestPawnShooter = Cast<AShooterCharacter>(BestPawn);
		SetIdol(BestPawnShooter);
		SetFavor(BestUtility);
		SetRelativeFavor(BestUtility - SecondBestUtility);
	}
	/*FString NewString = FString::FromInt(TimeWithIdol);
	FString VeryCleanString = FString::SanitizeFloat(TimeWithIdol);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, VeryCleanString);*/
}

void AFanAIController::SetFavor(float Utility)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsFloat(FavorID, Utility);
	}
}

void AFanAIController::SetRelativeFavor(float Utility)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsFloat(RelativeFavorID, Utility);
	}
}

void AFanAIController::SetIdol(class APawn* InPawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsObject(IdolID, InPawn);
		SetFocus(InPawn);
	}
}

void AFanAIController::SetCombatQuarter(class APawn* InPawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsObject(CombatQuarterID, InPawn);
		SetFocus(InPawn);
	}
}

class APawn* AFanAIController::GetCombatQuarter()
{
	if (BlackboardComp)
	{
		return Cast<APawn>(BlackboardComp->GetValueAsObject(CombatQuarterID));
	}
	return NULL;
}

class AShooterCharacter* AFanAIController::GetIdol() const
{
	if (BlackboardComp)
	{
		return Cast<AShooterCharacter>(BlackboardComp->GetValueAsObject(IdolID));
	}
	return NULL;
}

float AFanAIController::GetFavor() const
{
	if (BlackboardComp)
	{
		return BlackboardComp->GetValueAsFloat(FavorID);
	}
	return 0;
}

float AFanAIController::GetRelativeFavor() const
{
	if (BlackboardComp)
	{
		return BlackboardComp->GetValueAsFloat(RelativeFavorID);
	}
	return 0;
}

//void AFanAIController::FindClosestEnemy()
//{
//	APawn* MyBot = GetPawn();
//	if (MyBot == NULL)
//	{
//		return;
//	}
//
//	const FVector MyLoc = MyBot->GetActorLocation();
//	float BestDistSq = MAX_FLT;
//	ACombatant* BestPawn = NULL;
//
//	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
//	{
//		ACombatant* TestPawn = Cast<ACombatant>(*It);
//		if (TestPawn && TestPawn->IsAlive() && TestPawn->IsEnemyFor(this))
//		{
//			const float DistSq = (TestPawn->GetActorLocation() - MyLoc).SizeSquared();
//			if (DistSq < BestDistSq)
//			{
//				BestDistSq = DistSq;
//				BestPawn = TestPawn;
//			}
//		}
//	}
//
//	if (BestPawn)
//	{
//		AShooterCharacter *BestPawnShooter = Cast<AShooterCharacter>(BestPawn);
//		SetEnemy(BestPawnShooter);
//	}
//}

//bool AFanAIController::FindClosestEnemyWithLOS(AShooterCharacter* ExcludeEnemy)
//{
//	bool bGotEnemy = false;
//	APawn* MyBot = GetPawn();
//	if (MyBot != NULL)
//	{
//		const FVector MyLoc = MyBot->GetActorLocation();
//		float BestDistSq = MAX_FLT;
//		ACombatant* BestPawn = NULL;
//
//		for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
//		{
//			ACombatant* TestPawn = Cast<ACombatant>(*It);
//			if (TestPawn && TestPawn != ExcludeEnemy && TestPawn->IsAlive() && TestPawn->IsEnemyFor(this))
//			{
//				if (HasWeaponLOSToEnemy(TestPawn, true) == true)
//				{
//					const float DistSq = (TestPawn->GetActorLocation() - MyLoc).SizeSquared();
//					if (DistSq < BestDistSq)
//					{
//						BestDistSq = DistSq;
//						BestPawn = TestPawn;
//					}
//				}
//			}
//		}
//		if (BestPawn)
//		{
//			AShooterCharacter *BestPawnShooter = Cast<AShooterCharacter>(BestPawn);
//			SetEnemy(BestPawnShooter);
//			bGotEnemy = true;
//		}
//	}
//	return bGotEnemy;
//}

//bool AFanAIController::HasWeaponLOSToEnemy(AActor* InEnemyActor, const bool bAnyEnemy) const
//{
//	static FName LosTag = FName(TEXT("AIWeaponLosTrace"));
//
//	AShooterBot* MyBot = Cast<AShooterBot>(GetPawn());
//
//	bool bHasLOS = false;
//	// Perform trace to retrieve hit info
//	FCollisionQueryParams TraceParams(LosTag, true, GetPawn());
//	TraceParams.bTraceAsyncScene = true;
//
//	TraceParams.bReturnPhysicalMaterial = true;
//	FVector StartLocation = MyBot->GetActorLocation();
//	StartLocation.Z += GetPawn()->BaseEyeHeight; //look from eyes
//
//	FHitResult Hit(ForceInit);
//	const FVector EndLocation = InEnemyActor->GetActorLocation();
//	GetWorld()->LineTraceSingle(Hit, StartLocation, EndLocation, COLLISION_WEAPON, TraceParams);
//	if (Hit.bBlockingHit == true)
//	{
//		// Theres a blocking hit - check if its our enemy actor
//		AActor* HitActor = Hit.GetActor();
//		if (Hit.GetActor() != NULL)
//		{
//			if (HitActor == InEnemyActor)
//			{
//				bHasLOS = true;
//			}
//			else if (bAnyEnemy == true)
//			{
//				// Its not our actor, maybe its still an enemy ?
//				ACharacter* HitChar = Cast<ACharacter>(HitActor);
//				if (HitChar != NULL)
//				{
//					AShooterPlayerState* HitPlayerState = Cast<AShooterPlayerState>(HitChar->PlayerState);
//					AShooterPlayerState* MyPlayerState = Cast<AShooterPlayerState>(PlayerState);
//					if ((HitPlayerState != NULL) && (MyPlayerState != NULL))
//					{
//						if (HitPlayerState->GetTeamNum() != MyPlayerState->GetTeamNum())
//						{
//							bHasLOS = true;
//						}
//					}
//				}
//			}
//		}
//	}
//
//
//
//	return bHasLOS;
//}

//void AFanAIController::ShootEnemy()
//{
//	AShooterBot* MyBot = Cast<AShooterBot>(GetPawn());
//	AShooterWeapon* MyWeapon = MyBot ? MyBot->GetWeapon() : NULL;
//	if (MyWeapon == NULL)
//	{
//		return;
//	}
//
//	bool bCanShoot = false;
//	AShooterCharacter* Enemy = GetEnemy();
//	if (Enemy && (Enemy->IsAlive()) && (MyWeapon->GetCurrentAmmo() > 0) && (MyWeapon->CanFire() == true))
//	{
//		if (LineOfSightTo(Enemy, MyBot->GetActorLocation()))
//		{
//			bCanShoot = true;
//		}
//	}
//
//	if (bCanShoot)
//	{
//		MyBot->StartWeaponFire();
//	}
//	else
//	{
//		MyBot->StopWeaponFire();
//	}
//}
//
//void AFanAIController::CheckAmmo(const class AShooterWeapon* CurrentWeapon)
//{
//	if (CurrentWeapon && BlackboardComp)
//	{
//		const int32 Ammo = CurrentWeapon->GetCurrentAmmo();
//		const int32 MaxAmmo = CurrentWeapon->GetMaxAmmo();
//		const float Ratio = (float)Ammo / (float)MaxAmmo;
//
//		BlackboardComp->SetValueAsBool(NeedAmmoKeyID, (Ratio <= 0.1f));
//	}
//}
//

void AFanAIController::UpdateControlRotation(float DeltaTime, bool bUpdatePawn)
{
	// Look toward focus
	FVector FocalPoint = GetFocalPoint();
	if (!FocalPoint.IsZero() && GetPawn())
	{
		FVector Direction = FocalPoint - GetPawn()->GetActorLocation();
		FRotator NewControlRotation = Direction.Rotation();

		NewControlRotation.Yaw = FRotator::ClampAxis(NewControlRotation.Yaw);

		SetControlRotation(NewControlRotation);

		APawn* const P = GetPawn();
		if (P && bUpdatePawn)
		{
			P->FaceRotation(NewControlRotation, DeltaTime);
		}

	}
}

void AFanAIController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	// Stop the behaviour tree/logic
	BehaviorComp->StopTree();

	// Stop any movement we already have
	StopMovement();

	//Cancel the repsawn timer
	GetWorldTimerManager().ClearTimer(this, &AFanAIController::RespawnFan);

	// Clear any enemy
	//SetEnemy(NULL);

	// Finally stop firing
	/*AShooterBot* MyBot = Cast<AShooterBot>(GetPawn());
	AShooterWeapon* MyWeapon = MyBot ? MyBot->GetWeapon() : NULL;
	if (MyWeapon == NULL)
	{
		return;
	}
	MyBot->StopWeaponFire();*/
}




