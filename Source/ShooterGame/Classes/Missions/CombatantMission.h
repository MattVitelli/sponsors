// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ShooterGame.h"
#include "Core.h"
#include "Object.h"
#include "CombatantGoal.h"
#include "../Combatants/Combatant.h"
#include "../Combatants/Alliance.h"
#include "CombatantMission.generated.h"

/**
 * 
 */
UCLASS(CustomConstructor)
class SHOOTERGAME_API UCombatantMission : public UObject
{
	GENERATED_BODY()

public:

	UCombatantMission(const FObjectInitializer& ObjectInitializer);
	
	void addGoal(UCombatantGoal* newGoal) { goals.Add(newGoal); };
	TArray<UCombatantGoal*> getGoals() { return goals; };

	void addTeammate(ACombatant* newTeammate) { team.Add(newTeammate); };
	TArray<ACombatant*> getTeam() { return team; };

	int getStartingTeamSize() { return teamSize; };
	int getCurrentTeamSize() { return team.Num(); };
	int getMinTeamSize() { return minTeamSize; };
	float getMinHealth(){ return minimumHealth; };

	float rawUtilPoints(AllianceAttributeType pointType) { return computedUtilPoints.vec[pointType]; };

	DECLARE_EVENT_OneParam(UCombatantMission, FMissionFailed, UCombatantMission*);
	FMissionFailed MissionFailed;

	DECLARE_EVENT_OneParam(UCombatantMission, FMissionSucceeded, UCombatantMission*);
	FMissionSucceeded MissionSucceeded;

private:
	TArray<UCombatantGoal*> goals;
	TArray<ACombatant*> team;

	UtilPointVector computedUtilPoints;
	virtual void recomputeUtilPoints();

	int teamSize;
	int minTeamSize;
	float minimumHealth;
};
