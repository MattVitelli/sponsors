// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ShooterGame.h"
#include "Object.h"
#include "../Combatants/Alliance.h"
#include "CombatantGoal.generated.h"

/**
 * 
 */
UCLASS(Abstract, CustomConstructor)
class SHOOTERGAME_API UCombatantGoal : public UObject
{
	GENERATED_BODY()
	
public:

	UCombatantGoal(const FObjectInitializer& ObjectInitializer);
	
	float rawUtilPoints(AllianceAttributeType pointType) { return computedUtilPoints.vec[pointType]; };

	DECLARE_EVENT_OneParam(UCombatantGoal, FOnGoalSuccess, UCombatantGoal* /* the goal */);
	FOnGoalSuccess OnGoalSuccess;

	DECLARE_EVENT_OneParam(UCombatantGoal, FOnGoalFailure, UCombatantGoal* /* the goal */);
	FOnGoalSuccess OnGoalFailure;

protected:
	UtilPointVector computedUtilPoints;

	virtual void recomputeUtilPoints() {};
	virtual void optimizeForAlliance(AAlliance* alliance) {};
};
