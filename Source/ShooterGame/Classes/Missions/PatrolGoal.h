// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Combatants/Alliance.h"
#include "CombatantGoal.h"
#include "Engine.h"
#include "PatrolGoal.generated.h"

/**
 * 
 */
UCLASS(CustomConstructor)
class SHOOTERGAME_API UPatrolGoal : public UCombatantGoal
{
	GENERATED_BODY()

public:
	UPatrolGoal(const FObjectInitializer& ObjectInitializer);

	AAlliance* myAlliance;
	FVector patrolPosition;

	void optimize();

	FVector positionToPatrol() { return patrolPosition; };

protected:
	virtual void optimizeForAlliance(AAlliance* alliance);
	virtual void recomputeUtilPoints();
};
