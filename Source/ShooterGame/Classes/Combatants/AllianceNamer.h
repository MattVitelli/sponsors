// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ShooterGame.h"
#include "Core.h"
#include "Alliance.h"

/**
 * 
 */
class SHOOTERGAME_API AllianceNamer
{
public:
	AllianceNamer();
	~AllianceNamer();

	static FString getAdj(AllianceAttributeType type);
	static FString getNoun(AllianceAttributeType type);
};
