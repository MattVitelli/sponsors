// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "Goal.generated.h"

enum GoalState
{
    IN_PROGRESS = 0,
    FAILED,
    SUCCEEDED
};

UCLASS()
class UGoal : public UActorComponent
{
	GENERATED_UCLASS_BODY()

    virtual GoalState GetGoalState() const;
    
    virtual void OnGoalSuccess();
    
    virtual void OnGoalFailure();
};