// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "FindPointNearIdol.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UFindPointNearIdol : public UBTTask_BlackboardBase
{
	GENERATED_UCLASS_BODY()
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory) override;
	
};