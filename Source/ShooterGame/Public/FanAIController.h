// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "FanAIController.generated.h"

class UBehaviorTreeComponent;
class UBlackboardComponent;
/**
 * 
 */
UCLASS(config=Game)
class SHOOTERGAME_API AFanAIController : public AAIController
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY(transient)
	UBlackboardComponent* BlackboardComp;

	/* Cached BT component */
	UPROPERTY(transient)
	UBehaviorTreeComponent* BehaviorComp;

	TArray<ACombatant *> ShotByCombatants;

public:

	void ShotByCombatant(ACombatant *combatant);

	// Begin AController interface
	virtual void GameHasEnded(class AActor* EndGameFocus = NULL, bool bIsWinner = false) override;
	virtual void Possess(class APawn* InPawn) override;
	virtual void BeginInactiveState() override;
	// End APlayerController interface

	void RespawnFan();

	//void CheckAmmo(const class AShooterWeapon* CurrentWeapon);

	void SetIdol(class APawn* InPawn);

	UFUNCTION(BlueprintCallable, Category = Behavior)
	void SetCombatQuarter(class APawn* InPawn);

	class APawn* GetCombatQuarter();

	void SetFavor(float Utility);
	void SetRelativeFavor(float Utility);

	class AShooterCharacter* GetIdol() const;
	float GetFavor() const;
	float GetRelativeFavor() const;

	///* If there is line of sight to current enemy, start firing at them */
	//UFUNCTION(BlueprintCallable, Category = Behavior)
	//void ShootEnemy();

	UFUNCTION(BlueprintCallable, Category = Behavior)
	void FindFavoriteIdol();

	///* Finds the closest enemy and sets them as current target */
	//UFUNCTION(BlueprintCallable, Category = Behavior)
	//void FindClosestEnemy();

	/*UFUNCTION(BlueprintCallable, Category = Behavior)
	bool FindClosestEnemyWithLOS(AShooterCharacter* ExcludeEnemy);
*/
	//bool HasWeaponLOSToEnemy(AActor* InEnemyActor, const bool bAnyEnemy) const;

	// Begin AAIController interface
	/** Update direction AI is looking based on FocalPoint */
	virtual void UpdateControlRotation(float DeltaTime, bool bUpdatePawn = true) override;
	// End AAIController interface

protected:
	// Check of we have LOS to a character
	bool LOSTrace(AShooterCharacter* InEnemyChar) const;

	int32 IdolID;
	int32 FavorID;
	int32 RelativeFavorID;
	int32 IdolIsDyingID;
	int32 CombatQuarterID;
	float TimeWithIdol;
	//int32 NeedAmmoKeyID;

public:
	/** Returns BlackboardComp subobject **/
	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
	/** Returns BehaviorComp subobject **/
	FORCEINLINE UBehaviorTreeComponent* GetBehaviorComp() const { return BehaviorComp; }
};
