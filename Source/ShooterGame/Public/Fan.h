// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Player/ShooterCharacter.h"
#include "Fan.generated.h"

/**
 * 
 */
UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EFAN_TYPE : uint8
{
	EF_Aggressive 	UMETA(DisplayName = "Aggressive"),
	EF_Just 		UMETA(DisplayName = "Just"),
	EF_Lone			UMETA(DisplayName = "Lone")
};

UCLASS()
class SHOOTERGAME_API AFan : public AShooterCharacter
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* FanBehavior;

	virtual bool IsFirstPerson() const override;

	virtual void FaceRotation(FRotator NewRotation, float DeltaTime = 0.f) override;

public:

	UPROPERTY(EditAnywhere, Category = Behavior)
	EFAN_TYPE FAN_TYPE;

	UPROPERTY(EditAnywhere, Category = Behavior)
	float W_DESIRED_FEATURE;

	UPROPERTY(EditAnywhere, Category = Behavior)
	float W_FAN_BALANCE;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

};
